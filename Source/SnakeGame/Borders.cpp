// Fill out your copyright notice in the Description page of Project Settings.


#include "Borders.h"
#include "SnakeBase.h"

// Sets default values
ABorders::ABorders()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABorders::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABorders::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABorders::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake)) {
		Snake->Destroy();
	}
}

