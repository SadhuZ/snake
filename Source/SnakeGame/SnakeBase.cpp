// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	LastMoveDirection = EMovementDirection::DOWN;
	MovementTimeInterval = 0.5f;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementTimeInterval);
	AddSnakeElement(5);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	FVector NewLocation(0, 0, 0);
	if (SnakeElements.Num()) {
		NewLocation = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();
	}
	for (int i = 0; i < ElementsNum; i++) {
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElementIndex = SnakeElements.Add(NewSnakeElem);
		if (ElementIndex == 0) {
			NewSnakeElem->SetFirstElementType();
		}
	}
	
}

void ASnakeBase::Move()
{
	FVector MovementVector(0, 0, 0);
	float MovementStep = ElementSize;

	if (LastMoveDirection == EMovementDirection::UP) {
		MovementVector.X += MovementStep;
	}
	else if (LastMoveDirection == EMovementDirection::DOWN) {
		MovementVector.X -= MovementStep;
	}
	else if (LastMoveDirection == EMovementDirection::LEFT) {
		MovementVector.Y += MovementStep;
	}
	else if (LastMoveDirection == EMovementDirection::RIGHT) {
		MovementVector.Y -= MovementStep;
	} 

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; --i) {
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevElementLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevElementLocation);
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement)) {
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface) {
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

